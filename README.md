# Grupo 2 - Tarea Grupal - ComIT NodeJS

**Nombre**: Luyamar

**Integrantes**: 
- Lucia Rizzo
- Yanina Chaves
- Marcio Garozzo

**Tarea**: Armar un sitio web

**Requerimientos**
- Alimentada por una api
- 5 elementos de bootstrap: (Carousel, accordion, etc)
- 3 botones con diferentes eventos.
- Cards -> pasar por arriba del mouse, y que brinde informacion (Ya sea con un popover o dando vuelta la card)

*ComIT 2021*